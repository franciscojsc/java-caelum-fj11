package br.com.franciscochaves.caelum.chat.leituraescritadearquivo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

	public static void main(String[] args) throws IOException {
		ServerSocket servidor = new ServerSocket(12345);//porta para comunicção
		System.out.println("Porta 12345 aberta!");
		
		Socket cliente = servidor.accept();
		System.out.println("Nova conexão com o cliente "+
				cliente.getInetAddress().getHostAddress());
		
		//criar um arquivo para escrever o texto 
		OutputStream os = new FileOutputStream("saida.txt",true);
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);
		
		Scanner entrada = new Scanner(cliente.getInputStream());
		
		//escrever no arquivo texto
		while (entrada.hasNextLine()) {
			bw.write(entrada.nextLine());
			bw.newLine();
			
		}
		
		bw.close();
		
		entrada.close();
		servidor.close();
		
		
		//escreve na saida do sistema
		BufferedReader bf = new BufferedReader( 
				new InputStreamReader(
						new FileInputStream("saida.txt")));
		
		String lerArquivo = bf.readLine();
		
		while (lerArquivo != null) {
			System.out.println(lerArquivo);
			lerArquivo = bf.readLine();
		}
		
		bf.close();
	}
}
