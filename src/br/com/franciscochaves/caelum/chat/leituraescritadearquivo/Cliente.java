package br.com.franciscochaves.caelum.chat.leituraescritadearquivo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;


public class Cliente {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket cliente = new Socket("127.0.0.1", 12345);
		System.out.println("O cliente se conectou ao servidor!");
		
		InputStream file = new FileInputStream("documento.txt");
		InputStreamReader stream = new InputStreamReader(file);
		BufferedReader reader = new BufferedReader(stream);
		
		String lerArquivo = reader.readLine();
		
		PrintStream saida = new PrintStream(cliente.getOutputStream());
		
		while (lerArquivo != null) {
			saida.println(lerArquivo);
			lerArquivo = reader.readLine();
		}
				
		
		saida.close();
		reader.close();

	}
}
