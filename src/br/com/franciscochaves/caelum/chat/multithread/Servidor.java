package br.com.franciscochaves.caelum.chat.multithread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Servidor {

	public Servidor() throws IOException {
		ServerSocket servidor;
		System.out.println("Porta 12345 aberta!");
		
		
		while (true) {
			servidor = new ServerSocket(12345);
			
			try {
				while (true) {
					Socket socket = servidor.accept();
					new Thread(new EscutaCliente(socket)).start();
				}
			} catch (Exception e) {
			}
			servidor.close();

		}
	}

	private class EscutaCliente implements Runnable{
		Scanner leitor;
		public EscutaCliente(Socket socket) {
			
			try {
				leitor = new Scanner(socket.getInputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Override
		public void run() {
			String texto;
			while((texto = leitor.nextLine()) != null){
				System.out.println("Recebeu: "+ texto);
			}
		}
	}
	
	
	
	public static void main(String[] args) throws IOException {
		new Servidor();
	}
	
}
