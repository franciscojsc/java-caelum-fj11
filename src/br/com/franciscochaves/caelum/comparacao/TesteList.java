package br.com.franciscochaves.caelum.comparacao;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TesteList {

	public static void main(String[] args) {
		long inicio = System.currentTimeMillis();
		List<Aluno> lista = new LinkedList<>();

		Aluno aluno = new Aluno(19, "Junio");
		Aluno aluno1 = new Aluno(1, "Maria");
		Aluno aluno2 = new Aluno(18, "Francisco");
		Aluno aluno3 = new Aluno(32, "Nize");
		Aluno aluno4 = new Aluno(2, "Lele");
		Aluno aluno5 = new Aluno(9, "Sarava");
		Aluno aluno6 = new Aluno(73, "Tereza");
		Aluno aluno7 = new Aluno(22, "José");
		Aluno aluno8 = new Aluno(21, "Fernando");
		Aluno aluno9 = new Aluno(15, "Carlos");
		Aluno aluno10 = new Aluno(10, "Zé");

		lista.add(aluno);
		lista.add(aluno1);
		lista.add(aluno2);
		lista.add(aluno3);
		lista.add(aluno4);
		lista.add(aluno5);
		lista.add(aluno6);
		lista.add(aluno7);
		lista.add(aluno8);
		lista.add(aluno9);
		lista.add(aluno10);
		

		Collections.sort(lista);

		System.out.println(lista);

		for (int i = 0; i < lista.size(); i++) {
			System.out.println(lista.get(i).getNome() + " " + lista.get(i).getIdade());
		}
		System.out.println("Tempo Total: " + (System.currentTimeMillis() - inicio));
	}

}
