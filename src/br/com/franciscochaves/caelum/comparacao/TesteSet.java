package br.com.franciscochaves.caelum.comparacao;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TesteSet {

	public static void main(String[] args) {

		Set<String>listaSet = new HashSet<>();

		
		listaSet.add("Francisco");
		listaSet.add("Seilá");
		listaSet.add("Serve");
		listaSet.add("Pra que");
		listaSet.add("Nize");
		
		Iterator<String> iterator = listaSet.iterator(); 
		while(iterator.hasNext()){
			String palavra = iterator.next();
			System.out.println(palavra);
		}
		
		System.out.println(listaSet);
		
		
	}

}
