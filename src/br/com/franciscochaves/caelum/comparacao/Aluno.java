package br.com.franciscochaves.caelum.comparacao;

import java.util.Date;

public class Aluno implements Comparable<Aluno>{
	
	private String nome;
	private int idade;
	private Date data;
	
	public Aluno(int idade, String nome) {
		this.idade = idade;
		this.nome = nome;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	@Override
	public int compareTo(Aluno outro) {
		if(this.idade < outro.idade)
			return -1;
		if(this.idade > outro.idade)
			return 1;
		return 0;
	}
	@Override
	public String toString() {
		return "\nAluno [nome= " + nome + ", idade= " + idade + "]";
	}
	
	

}
