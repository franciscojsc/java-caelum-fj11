package br.com.franciscochaves.caelum.thread;

public class TesteThread {
	public static void main(String[] args) {
		Programa programa = new Programa();
		programa.setId(1);
		new Thread(programa).start();
		
		Programa programa1 = new Programa();
		programa1.setId(2);
		new Thread(programa1).start();
		
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 1000; i++) {
					System.out.println("Programa: " + 7+ "valor: "+ i);
				}				
			}
		};
		
		new Thread(runnable).start();
	}

}
